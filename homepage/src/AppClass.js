import React, {Component} from 'react';
import './App.css';

class AppClass extends Component{
    constructor(props){
        super(props);

        this.state={
            nama  : 'Syarah Syafitri',
            absen : '32'
        }
    }

    render(){
        return(
            <div>
                <h1 class="comp">Component Class</h1>
                <h2>Nama  : {this.state.nama}</h2>
                <h3>Absen : {this.state.absen}</h3>
            </div>
        );
    }
}

export default AppClass;